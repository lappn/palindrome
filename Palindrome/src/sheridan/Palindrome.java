package sheridan;

public class Palindrome {
	public static void main(String[] args){
		System.out.println("Is anna a palindrome?: " + isPalindrome("anna"));
		System.out.println("Is race car a palindrome?: " + isPalindrome("race car"));
		System.out.println("Is taco cat a palindrome?: " + isPalindrome("taco cat"));
		System.out.println("Is annb a palindrome? " + isPalindrome("annb"));
	}
	
	public static boolean isPalindrome(String input){
		boolean palindrome = false;
		String str = input.toLowerCase().replaceAll(" ","");
		for(int i = 0 , j =str.length() - 1; i < j - 1;i ++, j--){
			palindrome = str.charAt(i) == str.charAt(j);
		}
		return palindrome;
	}
	
}
